package org.java;

public class FatherClass {
	
	public void bmwCar() {
		System.out.println("Father's BMW car");
	}
	
	public void ktmBike() {
		System.out.println("Father's KTM bike");
	}

}
