package org.java;

public class ChildClass extends FatherClass {
	
	public void audiCar() {
		System.out.println("Son's Audi Car");
	}

	public void ktmBike() {
		System.out.println("son's KTM bike");
	}
	
	public static void main(String[] args) {
		ChildClass c = new ChildClass(); // Obj created for ChildClass
		c.bmwCar();
		c.audiCar();
		c.ktmBike();
		
		
		FatherClass f = new FatherClass();
		f.ktmBike();            //obj creater for father class
		
		
		FatherClass fc = new ChildClass();
		fc.ktmBike();           //Obj created for fatherclass and placed it in child class
	}
}
