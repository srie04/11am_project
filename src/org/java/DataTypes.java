package org.java;

public class DataTypes {
	public static void main(String[] args) {
		// Datatype var = value;
	
		byte age = 29;
		System.out.println("Age is "+age);
		
		short passedOutYear = 2015;
		System.out.println("Passedout Year is "+passedOutYear);
		
		int expectedSalary = 50000;
		System.out.println("Expected Salary is "+expectedSalary);
		
		long phoneNumber = 9876543210l;
		System.out.println("Phone number is "+phoneNumber);
		
		float height = 170.32f;
		System.out.println("Height is "+height);
		
		double random = 123.35436253157453423;
		System.out.println("Random number is "+random);
		
		char initial = 'A';
		System.out.println("Initial is "+initial);
		
		boolean maritalStatus = false;
		System.out.println("Marital Status is "+maritalStatus);
		
		String address = "22/54, MGR NAGAR, Chennai 600078 ";
		System.out.println("Address is "+address);
		
	}

}
