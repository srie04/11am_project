package org.java;

import java.util.Scanner;

public class ScannerClass {
	int a = 10 ;
	int b = 20 ;
	
	private void addition() {
		System.out.println(a+b);

	}
	
	private void addition(int a, int b) {
		System.out.println(a+b);
	}
	
	private void addition(byte a, byte b) {
		System.out.println(a+b);
	}
	
	private void addition(int a, byte b) {
		System.out.println(a+b);
	}
	
	private void addition(byte a , int b) {
		System.out.println(a+b);
	}
	
	private void addition(int a) {
		System.out.println(a);
	}
	
	public static void main(String[] args) {
	// ClassName objname = new ClassName();	
		ScannerClass s = new ScannerClass();
		//method calling
		s.addition();
		s.addition(2, 4);
		s.addition(3, 10);
		s.addition(30, 2);
		s.addition(13, 40);
		s.addition(20);
	}
	
}