package org.java;

public class Array_Class {

	//datatype var[] = new datatype[size];
	public static void main(String[] args) {
		//Datatype var = value; 
		String s = "Dell CompanyC123";
		//          01
		int length = s.length();
		System.out.println(length);
		
		boolean empty = s.isEmpty();
		System.out.println(empty); //false
		
		char charAt = s.charAt(1);
		System.out.println(charAt);
		
		int indexOf = s.indexOf("C");
		System.out.println(indexOf);
		
		int lastIndexOf = s.lastIndexOf("C");
		System.out.println(lastIndexOf);    //
		
		String upperCase = s.toUpperCase();
		System.out.println(upperCase);   //Captial letters
		
		String lowerCase = s.toLowerCase();
		System.out.println(lowerCase);
		
		boolean startsWith = s.startsWith("D");
		System.out.println(startsWith);
		
		boolean endsWith = s.endsWith("3");
		System.out.println(endsWith);
		
		boolean contains = s.contains("z");
		System.out.println(contains);
		
		String s1 = "dell companyC123";
		
		boolean equals = s.equals(s1);
		System.out.println(equals);
		
		boolean equalsIgnoreCase = s.equalsIgnoreCase(s1);
		System.out.println(equalsIgnoreCase);
		
		String concat = s.concat(s1);
		System.out.println(concat);
		
		String replace = s.replace("l", "z");
		System.out.println(replace);
		
		String replaceAll = s.replaceAll(s, s1);
		System.out.println(replaceAll);
		
		String trim = s.trim();
		System.out.println(trim);
		
		String[] split = s.split(s);
		//s = "Dell Company"
		//split = [D,e,l,l, ,C,o,m,p,a,n,y]
		for (String string : split) {
			System.out.println(string);
		}
		
		String substring = s.substring(2, 15);
		System.out.println(substring);
		
		
		
		
		System.out.println("GIT DEMO");
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
}
	
}
