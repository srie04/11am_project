package org.java;

public class FriendHouse {

	private int lockerAmt = 500000;

	public int getLockerAmt() {
		
		return lockerAmt;
	}
	
	
	public void setLockerAmt(int lockerAmt) {
		this.lockerAmt = lockerAmt;
	}
}
